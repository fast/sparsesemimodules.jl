
using LinearAlgebra
using SparseArrays
using SparseSemimodules
using Semirings
using Test
using CUDA

const SA = SparseArrays
const SSM = SparseSemimodules

const Ks = [ProbSemiring{Float32}, ProbSemiring{Float64}]

@testset "SparseVector" begin
    for K in Ks
        x = SSM.sparsevec([4, 2], [one(K) + one(K), one(K)], 5)
        u = SA.sparsevec([4, 2], [one(K) + one(K), one(K)], 5)
        I_x, V_x = SSM.findnz(x)
        I_u, V_u = SA.findnz(u)
        @test all(x .≈ u)

        x = SSM.sparsevec([2, 4], [one(K) + one(K), one(K)])
        u = SA.sparsevec([2, 4], [one(K) + one(K), one(K)])
        @test all(x .≈ u)
        @test length(x) == length(u)

        x = SSM.sparsevec([2, 4], one(K), 5)
        u = SA.sparsevec([2, 4], one(K), 5)
        @test all(x .≈ u)
        @test length(x) == length(u)

        x = SSM.sparsevec([2, 4], one(K))
        u = SA.sparsevec([2, 4], one(K))
        @test all(x .≈ u)
        @test length(x) == length(u)
    end
end

@testset "SparseMatrixCSR" begin
    for K in Ks
        X = SSM.sparse([4, 2], [1, 3], [one(K) + one(K), one(K)], 5, 3)
        U = SA.sparse([4, 2], [1, 3], [one(K) + one(K), one(K)], 5, 3)
        @test all(X .≈ U)
        @test size(X) == size(U)

        X = SSM.sparse([2, 4], [1, 3], [one(K) + one(K), one(K)])
        U = SA.sparse([2, 4], [1, 3], [one(K) + one(K), one(K)])
        @test all(X .≈ U)
        @test size(X) == size(U)

        X = SSM.sparse([2, 4], [1, 3], one(K), 5, 3)
        U = SA.sparse([2, 4], [1, 3], one(K), 5, 3)
        @test all(X .≈ U)
        @test size(X) == size(U)

        X = SSM.sparse([2, 4], [1, 3], one(K))
        U = SA.sparse([2, 4], [1, 3], one(K))
        @test all(X .≈ U)
        @test size(X) == size(U)

        #A = SSM.sparse([2, 4, 7, 9], [1, 3, 2, 5], one(K), 10, 6)
        #B = SSM.sparse([1, 3, 4], [2, 2, 3], one(K), 4, 3)
        #AB = SSM.blockdiag(A, B)
        #Z1 = SSM.spzeros(eltype(A), size(A,1), size(B,2))
        #Z2 = SSM.spzeros(eltype(A), size(B,1), size(A,2))
        #@test all(AB .≈ [A Z1 ; Z2 B])
    end
end

#@testset "SparseSMMatrixCSC" begin
#    for K in Ks
#        X = SSM.sparse([4, 2], [1, 3], [one(K) + one(K), one(K)], 5, 3)
#        U = SA.sparse([4, 2], [1, 3], [one(K) + one(K), one(K)], 5, 3)
#        I_X, V_X = SSM.findnz(X)
#        I_U, V_U = SA.findnz(U)
#        @test all(X .≈ U)
#        @test size(X) == size(U)
#        @test all(I_X .== I_U)
#        @test all(V_X .≈ V_U)
#
#        X = SSM.sparse([2, 4], [1, 3], [one(K) + one(K), one(K)])
#        U = SA.sparse([2, 4], [1, 3], [one(K) + one(K), one(K)])
#        @test all(X .≈ U)
#        @test size(X) == size(U)
#
#        X = SSM.sparse([2, 4], [1, 3], one(K), 5, 3)
#        U = SA.sparse([2, 4], [1, 3], one(K), 5, 3)
#        @test all(X .≈ U)
#        @test size(X) == size(U)
#
#        X = SSM.sparse([2, 4], [1, 3], one(K))
#        U = SA.sparse([2, 4], [1, 3], one(K))
#        @test size(X) == size(U)
#        @test all(X .≈ U)
#
#        x = SSM.sparsevec([2, 4], one(K), 5)
#        A = SSM.sparse([2, 4, 2, 4], [1, 1, 2, 2], one(K), 5, 2)
#        X = SSM.hcat(x, x)
#        Y = SSM.reduce(hcat, [x, x])
#        @test all(A .≈ X)
#        @test all(A .≈ Y)
#
#        x = SSM.sparsevec([2, 4], one(K), 5)
#        A = SSM.sparse([2, 4, 7, 9], [1, 1, 1, 1], one(K), 10, 1)
#        X = SSM.vcat(x, x)
#        Y = SSM.reduce(vcat, [x, x])
#        @test all(A .≈ X)
#        @test all(A .≈ Y)
#
#        Z = SSM.spzeros(eltype(A), size(A)...)
#        @test all(SSM.blockdiag(A, A) .≈ SSM.vcat(SSM.hcat(A, Z), SSM.hcat(Z, A)))
#        @test all(SSM.blockdiag(A, A) .≈ [A Z ; Z A])
#    end
#end


#@testset "semimodule (vector)" begin
#    for K in Ks
#        a = one(K) + one(K)
#        x = SSM.sparsevec([2, 4], one(K), 5)
#        y = SSM.sparsevec([2, 4], a, 5)
#        z = SSM.sparsevec([2, 4], one(K) + a, 5)
#        @test all((a * x) .≈ y)
#        @test all((x * a) .≈ y)
#        @test all(x + y .≈ z)
#    end
#end
#
#@testset "semimodule (matrix)" begin
#    for K in Ks
#        a = one(K) + one(K)
#        X = SSM.sparse([2, 4], [1, 3], one(K), 5, 5)
#        Y = SSM.sparse([2, 4], [1, 3], a, 5, 5)
#        Z = SSM.sparse([2, 4], [1, 3], one(K) + a, 5, 5)
#        @test all((a * X) .≈ Y)
#        @test all((X * a) .≈ Y)
#        @test all(X + Y .≈ Z)
#    end
#end
#
#
#@testset "Basic Linear Algebra" begin
#    for K in Ks
#        x1 = [1, 2, 3, 4]
#        x2 = K[1, 2, 3, 4]
#        y1 = Vector{K}(undef, 3)
#        y2 = Vector{K}(undef, 3)
#        csc = SSM.sparse([1, 1, 3], [1, 2, 4], K[2, 5, 3], length(y1), length(x2))
#        csr = SSM.sparsecsr([1, 1, 3], [1, 2, 4], K[2, 5, 3], length(y2), length(x2))
#        y3 = SA.sparse([1, 1, 3], [1, 2, 4], [2, 5, 3])*x1
#
#        SSM._spmdv_csc!(y1, ⊗, csc.colptr,  csc.rowval, csc.nzval, x2)
#        SSM._spmdv_csr!(y2, ⊗, csr.rowptr, csr.colval, csr.nzval, x2)
#
#        @test all(y1 .≈ y2)
#        # @test all(y1 .≈ y3) #FIXME: This won't pass because of the semiring part
#    end
#end
#
#
#@testset "Linear Algebra" begin
#    for K in Ks
#        a = one(K) + one(K)
#        x = SSM.sparsevec([1, 2, 4], one(K), 5)
#        y = SSM.sparsevec([2, 4], a, 5)
#        @test SSM.dot(x, y) == sum(x[i] * y[i] for i in eachindex(x))
#        @test transpose(x) * y ≈ SSM.dot(x, y)
#
#        A = SSM.sparse([2, 4], [1, 3], a, 5, 5)
#        B = SA.sparse([2, 4], [1, 3], a, 5, 5)
#        u = SA.sparsevec([1, 2, 4], one(K), 5)
#        @test (A * x) isa SparseSMVector
#        @test (A * ones(K, length(x))) isa Vector
#        # @test all(A * x .≈ B * u) #FIXME: attempts to do K(::bool) at some point
#        @test all(transpose(A) * x .≈ transpose(B) * u)
#        @test all(transpose(x) * A .≈ transpose(u) * B)
#        # @test all(transpose(x) * transpose(A) .≈ transpose(u) * transpose(B)) #FIXME:
#        @test (transpose(x) * A) isa Transpose{K, <:SparseSMVector{K}}
#
#        V1 = SSM.sparsevec([1, 4, 5], [2, 6, 3], 6)
#        V2 = SSM.sparsevec([2, 4], [1, 5], 7)
#
#        @test all(SSM.kron(V1, V2) .== vcat([V1[i] .* V2 for i in eachindex(V1)]...))
#
#        M1 = SSM.sparse([1, 2, 4, 6, 8], [2, 3, 4, 2, 6], [2, 5, 1, 5, 9], 8, 7)
#        M1ᵀ = transpose(M1)
#        M2 = SSM.sparse([2, 3, 4, 2, 6], [1, 2, 4, 6, 8], [2, 5, 1, 5, 9], 7, 8)
#        @test all(M2 .== M1ᵀ)
#        @test size(M2) == size(M1ᵀ)
#
#        @test all(SSM.dot(V1, V2) .== sum([V1[i]*V2[i] for i in eachindex(V1)]))
#
#        V1 = SSM.sparsevec([1, 2, 4, 5], [2, 4, 6, 3], 7)
#        V2 = SSM.sparsevec([1, 3, 7], [2, 6, 3], 8)
#        @test all(M1 * V1 .== M1 * [2, 4, 0, 6, 3, 0, 0])
#        @test all(M2 * V2 .== M2 * [2, 0, 6, 0, 0, 0, 3, 0])
#    end
#end
#
#if(CUDA.functional())
#    CUDA.@allowscalar @testset "Parallel Linear Algebra" begin
#        for K in Ks
#            #= Variable initialization and equality test =#
#
#            M1 = SSM.sparse([1, 4, 5], [2, 3, 4], K[1, 2, 3], 5, 5)
#            M2 = SSM.sparse([1, 2, 3, 5], [1, 2, 4, 5], ones(K, 4), 5, 5)
#            cuM1 = cu(M1)
#            @test cuM1==M1
#            cuM2 = cu(M2)
#            @test cuM2==M2
#
#            M3 = sparsecsr([1, 2, 4, 5], [2, 4, 3, 4], K[1, 2, 3, 3], 5, 5)
#            M4 = sparsecsr([1, 3, 4, 5], [1, 2, 3, 4], ones(K, 4), 5, 5)
#            cuM3 = cu(M3)
#            @test cuM3==M3
#            cuM4 = cu(M4)
#            @test cuM4==M4
#
#            V1 = SSM.sparse(K[0, 1, 2, 0, 3])
#            V2 = SSM.sparse(K[2, 0, 0, 3, 1])
#            cuV1 = cu(V1)
#            @test cuV1==V1
#            cuV2 = cu(V2)
#            @test cuV2==V2
#
#            V3 = K[0, 1, 2, 0, 3]
#            V4 = K[2, 0, 0, 3, 1]
#            cuV3 = cu(V3)
#            @test cuV3==V3
#            cuV4 = cu(V4)
#            @test cuV4==V4
#
#            V1ᵀ = transpose(V1)
#            cuV1ᵀ = transpose(cuV1)
#            @test V1ᵀ==cuV1ᵀ
#
#            V2ᵀ = transpose(V2)
#            cuV2ᵀ = transpose(cuV2)
#            @test V2ᵀ==cuV2ᵀ
#
#            V3ᵀ = transpose(V3)
#            cuV3ᵀ = transpose(cuV3)
#            @test V3ᵀ==cuV3ᵀ
#
#            V4ᵀ = transpose(V4)
#            cuV4ᵀ = transpose(cuV4)
#            @test V4ᵀ==cuV4ᵀ
#
#            M5 = SSM.sparse([1, 4, 5], [2, 3, 4], K[1, 2, 3], 5, 6)
#            cuM5 = cu(M5)
#            @test cuM5==M5
#            M6 = SSM.sparse([1, 2, 3, 5], [1, 2, 4, 5], ones(K, 4), 6, 5)
#            cuM6 = cu(M6)
#            @test cuM6==M6
#            M7 = SSM.sparsecsr([1, 2, 4, 5], [2, 4, 3, 4], K[1, 2, 3, 3], 5, 4)
#            cuM7 = cu(M7)
#            @test cuM7==M7
#            M8 = SSM.sparsecsr([1, 3, 4, 3], [1, 2, 3, 4], ones(K, 4), 4, 6)
#            cuM8 = cu(M8)
#            @test cuM8==M8
#
#            #= Computations =#
#
#            @test cuM1*cuV1 == M1*V1
#            @test cuM1*cuV2 == M1*V2
#            @test cuM2*cuV1 == M2*V1
#            @test cuM2*cuV2 == M2*V2
#
#            @test cuM1*cuV3 == M1*V3
#            @test cuM1*cuV4 == M1*V4
#            @test cuM2*cuV3 == M2*V3
#            @test cuM2*cuV4 == M2*V4
#
#            @test cuM3*cuV1 == M3*V1
#            @test cuM3*cuV2 == M3*V2
#            @test cuM4*cuV1 == M4*V1
#            @test cuM4*cuV2 == M4*V2
#
#            @test cuM3*cuV3 == M3*V3
#            @test cuM3*cuV4 == M3*V4
#            @test cuM4*cuV3 == M4*V3
#            @test cuM4*cuV4 == M4*V4
#
#            @test cuM1*cuM1 == M1*M1
#            @test cuM1*cuM2 == M1*M2
#            @test cuM2*cuM1 == M2*M1
#            @test cuM2*cuM2 == M2*M2
#
#            @test cuM3*cuM3 == M3*M3
#            @test cuM3*cuM4 == M3*M4
#            @test cuM4*cuM3 == M4*M3
#            @test cuM4*cuM4 == M4*M4
#
#            @test transpose(cuV1)*cuM1 == transpose(V1)*M1
#            @test transpose(cuV2)*cuM1 == transpose(V2)*M1
#            @test transpose(cuV1)*cuM2 == transpose(V1)*M2
#            @test transpose(cuV2)*cuM2 == transpose(V2)*M2
#
#            @test transpose(cuV1)*cuM3 == transpose(V1)*M3
#            @test transpose(cuV2)*cuM3 == transpose(V2)*M3
#            @test transpose(cuV1)*cuM4 == transpose(V1)*M4
#            @test transpose(cuV2)*cuM4 == transpose(V2)*M4
#
#            @test cuM5*cuM6 == M5*M6
#            @test cuM7*cuM8 == M7*M8
#
#            @test dot(cuV1, cuV2)==dot(V1,V2)
#            @test cuV1ᵀ*cuV2==V1ᵀ*V2
#            @test dot(cuV3, cuV4)==dot(V3, V4)
#            @test cuV3ᵀ*cuV4==V3ᵀ*V4
#        end
#    end
#end
