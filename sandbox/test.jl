### A Pluto.jl notebook ###
# v0.19.24

using Markdown
using InteractiveUtils

# ╔═╡ df92cc62-e043-11ed-130a-23a9e5e27c26
begin
	using Pkg
	Pkg.activate("..")

	using LinearAlgebra
	using Revise
	using Semirings
	using SparseArrays
	const SA = SparseArrays
	using SparseSemimodules
	const SS = SparseSemimodules
	using Zygote
	using ChainRulesCore
	using CUDA
end

# ╔═╡ ebe9f527-15fe-4396-bcce-7db4e660dc97
K = Float32

# ╔═╡ 35d957b0-5bb5-4845-8940-3b83be649afb
A = SS.sparse([2, 2], [1, 3], one(K), 3, 4)

# ╔═╡ 945e9312-c6a8-4024-ba50-464e702af2cd
x = SS.sparsevec([2, 3], one(K))

# ╔═╡ b308d33a-7ee2-4988-aa7d-aa156e0b43bb
A * x

# ╔═╡ 067e5d11-52c6-4d64-b456-66086c7e01c0


# ╔═╡ af3ed776-a3f0-4d0d-ae43-a37069c93caf
transpose(x) * A

# ╔═╡ 16ce4d13-6190-4294-9048-2d3790bea773
x 

# ╔═╡ b729d4f4-883c-49e9-8553-b5fbf80eebc6
reduce(hcat, A + A)

# ╔═╡ 53e16ff6-092f-43fb-bcc5-57478885f683
A 

# ╔═╡ 03287185-6904-4a96-8bad-3ecbb4235ad2
A 

# ╔═╡ b99adbfb-005d-4fa8-b6e5-4ab92dd3a333
A * K(2)

# ╔═╡ 84dd5eb5-9383-4ad8-b686-24e53a5aac7d
vcat([1, 2], [3, 4])

# ╔═╡ 69f330a8-f0c1-49df-9f5c-1171789793ec
reduce(vcat, [[1,2], [3,4]])

# ╔═╡ 067a1542-eb34-46d3-a7ec-2012f410fa48
hcat(x, x) 	

# ╔═╡ a20c4d2f-3b6b-4fc6-be55-66e6b356bfd3
reduce(hcat, [x, x, x, x])

# ╔═╡ e40ccc3d-11d8-4b8b-ad1a-ad837dd7d02f
X = SS.sparse([2, 4], [1, 3], one(K), 5, 5)

# ╔═╡ d3519f93-348c-4ab6-85e4-55bdf38270e4
Y = SS.sparse([2, 4], [1, 3], one(K) ⊕ one(K), 5, 5)

# ╔═╡ 14b39c0b-5d14-4fa9-b836-2d8854a7b0c9
Z = SS.sparse([2, 4], [1, 3], one(K) + one(K) + one(K), 5, 5)

# ╔═╡ 180991d6-d758-4f40-942e-0440366b69ba
Semiring

# ╔═╡ d46b2551-03d0-4f3a-a43c-8b84534190e8
transpose(A) * x

# ╔═╡ 00d8f6e3-de4c-4adb-8491-7cdd45471531
transpose(x) * A

# ╔═╡ c073ea03-18fb-41b1-bb23-af6e9af2f06f
A, x

# ╔═╡ 124c5f7f-2f6b-40b2-bb1c-f5992177474e
X.colptr

# ╔═╡ 524ba81c-e631-4c02-bb90-d018057a76eb
X.rowval

# ╔═╡ 175aa0cf-8860-4436-a972-08adc5d2ae58
# ╠═╡ disabled = true
#=╠═╡
X = SparseSMMatrixCSC(3, 3, [1, 2, 4], [1, 3, 5], [2, 1, 3, 1, 2], [1, 2, 3, 4, 5])
  ╠═╡ =#

# ╔═╡ 68b7e107-a5c8-40c0-9b0c-70320bf8f92e
I, J, V = SS.findnz(X)

# ╔═╡ cbbb4785-1c07-4091-a917-fc981c4877b9
ii = getindex.(sort(CartesianIndex.(I, J), by = c -> c[2]), 2) 

# ╔═╡ 1a82d4aa-6350-44dd-adeb-afd7ba0d8a88
begin 
	rowstart = ones(Int, 3)
	rowend = zeros(Int, 3)
	rowstart[1] = 1
	
	for a in 2:length(ii)
		if ii[a] != ii[a-1]
			rowstart[ii[a]] = a
		end
	end

	for a in 1:(length(ii))
		if a == length(ii)
			rowend[ii[a]] = a
		elseif ii[a] != ii[a+1]
			rowend[ii[a]] = a
		end
	end

	rowstart, rowend
end

# ╔═╡ Cell order:
# ╠═df92cc62-e043-11ed-130a-23a9e5e27c26
# ╠═ebe9f527-15fe-4396-bcce-7db4e660dc97
# ╠═35d957b0-5bb5-4845-8940-3b83be649afb
# ╠═945e9312-c6a8-4024-ba50-464e702af2cd
# ╠═b308d33a-7ee2-4988-aa7d-aa156e0b43bb
# ╠═067e5d11-52c6-4d64-b456-66086c7e01c0
# ╠═af3ed776-a3f0-4d0d-ae43-a37069c93caf
# ╠═16ce4d13-6190-4294-9048-2d3790bea773
# ╠═b729d4f4-883c-49e9-8553-b5fbf80eebc6
# ╠═53e16ff6-092f-43fb-bcc5-57478885f683
# ╠═03287185-6904-4a96-8bad-3ecbb4235ad2
# ╠═b99adbfb-005d-4fa8-b6e5-4ab92dd3a333
# ╠═84dd5eb5-9383-4ad8-b686-24e53a5aac7d
# ╠═69f330a8-f0c1-49df-9f5c-1171789793ec
# ╠═067a1542-eb34-46d3-a7ec-2012f410fa48
# ╠═a20c4d2f-3b6b-4fc6-be55-66e6b356bfd3
# ╠═e40ccc3d-11d8-4b8b-ad1a-ad837dd7d02f
# ╠═d3519f93-348c-4ab6-85e4-55bdf38270e4
# ╠═14b39c0b-5d14-4fa9-b836-2d8854a7b0c9
# ╠═180991d6-d758-4f40-942e-0440366b69ba
# ╠═d46b2551-03d0-4f3a-a43c-8b84534190e8
# ╠═00d8f6e3-de4c-4adb-8491-7cdd45471531
# ╠═c073ea03-18fb-41b1-bb23-af6e9af2f06f
# ╠═124c5f7f-2f6b-40b2-bb1c-f5992177474e
# ╠═524ba81c-e631-4c02-bb90-d018057a76eb
# ╠═175aa0cf-8860-4436-a972-08adc5d2ae58
# ╠═68b7e107-a5c8-40c0-9b0c-70320bf8f92e
# ╠═cbbb4785-1c07-4091-a917-fc981c4877b9
# ╠═1a82d4aa-6350-44dd-adeb-afd7ba0d8a88
