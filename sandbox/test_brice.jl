### A Pluto.jl notebook ###
# v0.19.25

using Markdown
using InteractiveUtils

# ╔═╡ 6c217644-ef24-11ed-0764-5d0477579e04
begin
	using Pkg
	Pkg.activate("..")
	using Revise 
	using SparseSemimodules
	using Semirings
	#using SparseArrays
	using Test 
end

# ╔═╡ 1a4f3fea-8a96-4721-8504-989939c0f82b
K = ProbSemiring{Float32}

# ╔═╡ 0737e160-be5e-4e0a-872e-066234a1c8bc
begin
	m1 = 7
	n1 = 6 
	I1 = [1, 2, 3, 5, 7, 7, 1]
	J1 = [2, 3, 2, 5, 4, 6, 1]
	V1 = K[1, 2, 3, 4, 5, 6, 7] 
	M2 = sparsecsr(I1, J1, V1, m1, n1)
	M1 = sparse(I1, J1, V1, m1, n1)
end

# ╔═╡ 5e417908-3f5d-4ad7-ac0b-ca8e612713ad
begin
@show V2 = SparseSemimodules.sparsevec([1, 4, 5], K[2, 6, 3], 6)
@show V3 = SparseSemimodules.sparsevec([2, 4], K[1, 5], 7)
end

# ╔═╡ 10306ee8-08d1-4a54-9962-20ac0c6334eb
Vkron = kron(V2, V3)

# ╔═╡ ef3b53be-a80e-461a-8439-74967a336b7c
V2ᵀ = transpose(V2)

# ╔═╡ 7b60564c-f53a-4019-be8e-36fe241881e8
begin
	V4 = sparsevec([2, 3], K[4, 2], 3)
	V5 = sparsevec([1, 3], K[2, 3], 3)
	V4ᵀ = transpose(V4)
	V5ᵀ = transpose(V5)
end

# ╔═╡ 80c44015-d305-46cf-8fc0-cb65f59fac6d
vt = sparsevec([1, 2, 3, 4, 5, 6], K[1, 4, 5, 8, 2, 3], 6)

# ╔═╡ 7021a7ef-8bda-4db4-af45-0c96a6dc7e3d
M1 * vt

# ╔═╡ 7150f44d-4dbb-4f7b-a7fc-e838e42ac312
arr = K[2, 1, 2, 5, 4, 2]

# ╔═╡ b549bb41-d37e-41ea-a532-36aa3071fa11
M1 * arr

# ╔═╡ 0079caca-d794-49ea-b148-f1746c3b37c3
transpose(vt)*transpose(M1)

# ╔═╡ 2b71a092-9b35-47b3-8b0c-a949b0bdd6c8
M4 = sparse([1, 3, 5], [2, 2, 6], K[1, 7, 3] ,6, 7)

# ╔═╡ b7ad56d1-b918-4bd0-be9d-bdfd77830cbb
M1 * M4

# ╔═╡ f47cf6ec-4f9c-44f9-b670-379ab78f6f4b
transpose(M4)*transpose(M1)

# ╔═╡ 68749de8-4c1e-4882-9c9d-e9ac6d23384c
begin
	M5 = sparsecsr([1, 3], [2, 1], K[3, 4], 3, 3)
	M6 = sparsecsr([1, 2], [1, 1], K[2, 1], 3, 3)
end

# ╔═╡ 091c9ce1-59cd-4690-931f-4a1d97e10ceb
M5 * M6

# ╔═╡ f44f0d82-2373-4fa5-8405-b64f7ead540f
M4 + M4

# ╔═╡ 864d3f3a-b5a3-4c0e-b3f4-a7cf0356e9b8
M5 + M5

# ╔═╡ dde39034-dd3f-4655-b2d1-d881667fa34e
v = K[1, 3, 2, 0 ,0 ,1, 0 , 4]

# ╔═╡ b7b03f3a-74e3-4b75-b599-1b9dbc3c8921
X = sparse(v)

# ╔═╡ caed2f88-2bcd-4ae1-acb7-de9747e71f32


# ╔═╡ 244bb467-0925-41be-b7fa-1a3f7f6fa11e
isempty(searchsorted(X.nzind, 10))

# ╔═╡ 8dcd9e1a-9521-4f1f-af67-6e091a160d57


# ╔═╡ 3e99a5e6-bc05-4ea5-b648-d72c837c86f8
X.nzind

# ╔═╡ 8605fc17-c649-44a7-a6ea-4f3faa19d6f6
lastindex(X)

# ╔═╡ 30c4c8d2-52a5-4fce-9832-2d56766a0e0f
UInt(1) isa UInt

# ╔═╡ Cell order:
# ╠═6c217644-ef24-11ed-0764-5d0477579e04
# ╠═1a4f3fea-8a96-4721-8504-989939c0f82b
# ╠═0737e160-be5e-4e0a-872e-066234a1c8bc
# ╠═5e417908-3f5d-4ad7-ac0b-ca8e612713ad
# ╠═10306ee8-08d1-4a54-9962-20ac0c6334eb
# ╠═ef3b53be-a80e-461a-8439-74967a336b7c
# ╠═7b60564c-f53a-4019-be8e-36fe241881e8
# ╠═80c44015-d305-46cf-8fc0-cb65f59fac6d
# ╠═7021a7ef-8bda-4db4-af45-0c96a6dc7e3d
# ╠═7150f44d-4dbb-4f7b-a7fc-e838e42ac312
# ╠═b549bb41-d37e-41ea-a532-36aa3071fa11
# ╠═0079caca-d794-49ea-b148-f1746c3b37c3
# ╠═2b71a092-9b35-47b3-8b0c-a949b0bdd6c8
# ╠═b7ad56d1-b918-4bd0-be9d-bdfd77830cbb
# ╠═f47cf6ec-4f9c-44f9-b670-379ab78f6f4b
# ╠═68749de8-4c1e-4882-9c9d-e9ac6d23384c
# ╠═091c9ce1-59cd-4690-931f-4a1d97e10ceb
# ╠═f44f0d82-2373-4fa5-8405-b64f7ead540f
# ╠═864d3f3a-b5a3-4c0e-b3f4-a7cf0356e9b8
# ╠═dde39034-dd3f-4655-b2d1-d881667fa34e
# ╠═b7b03f3a-74e3-4b75-b599-1b9dbc3c8921
# ╠═caed2f88-2bcd-4ae1-acb7-de9747e71f32
# ╠═244bb467-0925-41be-b7fa-1a3f7f6fa11e
# ╠═8dcd9e1a-9521-4f1f-af67-6e091a160d57
# ╠═3e99a5e6-bc05-4ea5-b648-d72c837c86f8
# ╠═8605fc17-c649-44a7-a6ea-4f3faa19d6f6
# ╠═30c4c8d2-52a5-4fce-9832-2d56766a0e0f
