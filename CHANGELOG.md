# Releases

## [0.1.0] (https://gitlab.lisn.upsaclay.fr/fast/sparsesemimodules.jl/-/tree/v0.1.0) - 19/05/2023
### Added
- Abstract type SparseSMMatrix
- Row-compressed matrices SparseSMMatrixCSR
- Concatenation functions for SparseSMMatrix
- Kronecker product for SparseSMVector
- Diagonal block for SparseSMMatrix
- Copy constructor from Transpose form

### Changed
- SparseSMMatrixCSC is now a sub-type of SparseSMMatrix
- Operator *'s signature for SparseSMVector dot product has been specified